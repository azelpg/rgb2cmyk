/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_BUF_H
#define MLK_BUF_H

#ifdef __cplusplus
extern "C" {
#endif

void mBufInit(mBuf *p);
void mBufFree(mBuf *p);
mlkbool mBufAlloc(mBuf *p,mlksize allocsize,mlksize expand_size);

void mBufReset(mBuf *p);
void mBufSetCurrent(mBuf *p,mlksize pos);
void mBufBack(mBuf *p,int size);
mlksize mBufGetRemain(mBuf *p);
uint8_t *mBufGetCurrent(mBuf *p);

void mBufCutCurrent(mBuf *p);

mlkbool mBufAppend(mBuf *p,const void *buf,int size);
void *mBufAppend_getptr(mBuf *p,const void *buf,int size);
mlkbool mBufAppendByte(mBuf *p,uint8_t dat);
mlkbool mBufAppend0(mBuf *p,int size);
int32_t mBufAppendUTF8(mBuf *p,const char *buf,int len);

void *mBufCopyToBuf(mBuf *p);

#ifdef __cplusplus
}
#endif

#endif
