/*$
rgb2cmyk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

//================================
// イメージ
//================================

#include <stdio.h>
#include <string.h>
#include <lcms2.h>

#include "mlk.h"
#include "mlk_file.h"
#include "mlk_imagebuf.h"
#include "mlk_loadimage.h"
#include "mlk_saveimage.h"


#define IMAGESIZE_MAX  30000


/* RGBA -> RGBX 変換 */

static void _convert_rgba_to_rgb(mImageBuf2 *img)
{
	int ix,iy,a;
	uint8_t **ppbuf,*pd;

	ppbuf = img->ppbuf;

	for(iy = img->height; iy; iy--)
	{
		pd = *(ppbuf++);
	
		for(ix = img->width; ix; ix--, pd += 4)
		{
			a = pd[3];

			if(a != 255)
			{
				if(a == 0)
					pd[0] = pd[1] = pd[2] = 255;
				else
				{
					pd[0] = (pd[0] - 255) * a / 255 + 255;
					pd[1] = (pd[1] - 255) * a / 255 + 255;
					pd[2] = (pd[2] - 255) * a / 255 + 255;
				}
			}
		}
	}
}

/** RGB <-> CMYK 変換 */

void image_convert_colorspace(mImageBuf2 *img,cmsHTRANSFORM trans)
{
	int iy,w;
	uint8_t **ppbuf,*pd;

	ppbuf = img->ppbuf;
	w = img->width;

	for(iy = img->height; iy; iy--)
	{
		pd = *(ppbuf++);

		cmsDoTransform(trans, pd, pd, w);
	}
}

/** 画像ファイル読み込み */

mImageBuf2 *image_load(const char *filename,int *dst_dpi,uint8_t **prof_buf,uint32_t *prof_size)
{
	mLoadImage li;
	mLoadImageType type;
	mImageBuf2 *img = NULL;
	mFuncLoadImageCheck checks[] = {
		mLoadImage_checkPNG, mLoadImage_checkPSD,
		mLoadImage_checkTIFF, 0
	};
	int err,h,v;

	mLoadImage_init(&li);

	li.open.type = MLOADIMAGE_OPEN_FILENAME;
	li.open.filename = filename;
	li.convert_type = MLOADIMAGE_CONVERT_TYPE_RGBA;
	li.flags = MLOADIMAGE_FLAGS_TRANSPARENT_TO_ALPHA;

	//ヘッダからタイプ判定

	err = mLoadImage_checkFormat(&type, &li.open, checks, 0);
	if(err)
	{
		if(err == MLKERR_UNSUPPORTED)
			puts("! unsupported format");
		else
			puts("! load image error");

		return NULL;
	}

	//開く

	if((type.open)(&li)) goto ERR;

	//最大サイズ

	if(li.width > IMAGESIZE_MAX || li.height > IMAGESIZE_MAX)
		goto ERR;

	//画像確保

	img = mImageBuf2_new(li.width, li.height, 32, 0);
	if(!img) goto ERR;

	//解像度

	if(!mLoadImage_getDPI(&li, &h, &v))
		h = v = 0;

	*dst_dpi = h;

	//ICC プロファイル

	switch(type.format_tag)
	{
		//PNG
		case MLOADIMAGE_FORMAT_TAG_PNG:
			*prof_buf = mLoadImagePNG_getICCProfile(&li, prof_size);
			break;
		//TIFF
		case MLOADIMAGE_FORMAT_TAG_TIFF:
			*prof_buf = mLoadImageTIFF_getICCProfile(&li, prof_size);
			break;
		//PSD
		case MLOADIMAGE_FORMAT_TAG_PSD:
			*prof_buf = mLoadImagePSD_getICCProfile(&li, prof_size);
			break;
	}

	//イメージ読み込み

	li.imgbuf = img->ppbuf;

	if((type.getimage)(&li))
		goto ERR;

	//

	(type.close)(&li);

	//RGBA -> RGB

	_convert_rgba_to_rgb(img);

	return img;

	//エラー
ERR:
	(type.close)(&li);
	mImageBuf2_free(img);
	puts("! load image error");
	return NULL;
}


//========================
// 書き込み共通
//========================


/* Y1行イメージセット */

static mlkerr _save_setrow(mSaveImage *p,int y,uint8_t *buf,int line_bytes)
{
	memcpy(buf, *((uint8_t **)p->param1 + y), line_bytes);

	return MLKERR_OK;
}


//========================
// CMYK 書き込み
//========================


/* Y1行イメージセット (PSD) */

static mlkerr _save_cmyk_setrow_ch(mSaveImage *p,int y,int ch,uint8_t *buf,int line_bytes)
{
	uint8_t *ps;
	int i;

	ps = *((uint8_t **)p->param1 + y) + ch;

	for(i = p->width; i; i--, ps += 4)
		*(buf++) = *ps;

	return MLKERR_OK;
}

/** CMYK 画像書き込み
 *
 * type: 0=PSD, 1=TIFF
 * profile: NULL 以外でプロファイル埋め込み。ICC プロファイルのパス名 */

void image_save_cmyk(mImageBuf2 *img,const char *filename,
	int type,int dpi,int fnocomp,const char *profile)
{
	mSaveImage si;
	mSaveImageOpt opt;
	uint8_t *buf = NULL;
	int32_t profsize;
	int ret;

	mSaveImage_init(&si);

	si.open.type = MSAVEIMAGE_OPEN_FILENAME;
	si.open.filename = filename;
	si.width = img->width;
	si.height = img->height;
	si.coltype = MSAVEIMAGE_COLTYPE_CMYK;
	si.bits_per_sample = 8;
	si.samples_per_pixel = 4;
	si.setrow = _save_setrow;
	si.setrow_ch = _save_cmyk_setrow_ch;
	si.param1 = img->ppbuf;

	//解像度

	if(dpi)
	{
		si.reso_unit = MSAVEIMAGE_RESOUNIT_DPI;
		si.reso_horz = dpi;
		si.reso_vert = dpi;
	}

	//オプション

	opt.mask = 0;

	if(fnocomp)
	{
		if(type == 0)
		{
			//PSD
			opt.psd.mask = MSAVEOPT_PSD_MASK_COMPRESSION;
			opt.psd.compression = 0;
		}
		else
		{
			//TIFF
			opt.tiff.mask = MSAVEOPT_TIFF_MASK_COMPRESSION;
			opt.tiff.compression = MSAVEOPT_TIFF_COMPRESSION_NONE;
		}
	}

	if(profile)
	{
		if(mReadFileFull_alloc(profile, 0, &buf, &profsize) == MLKERR_OK)
		{
			if(type == 0)
			{
				//PSD
				opt.psd.mask |= MSAVEOPT_PSD_MASK_ICCPROFILE;
				opt.psd.profile_buf = buf;
				opt.psd.profile_size = profsize;
			}
			else
			{
				//TIFF
				opt.tiff.mask |= MSAVEOPT_TIFF_MASK_ICCPROFILE;
				opt.tiff.profile_buf = buf;
				opt.tiff.profile_size = profsize;
			}
		}
	}

	//保存

	if(type == 0)
		ret = mSaveImagePSD(&si, &opt);
	else
		ret = mSaveImageTIFF(&si, &opt);

	mFree(buf);

	//

	if(ret)
		puts("! save CMYK image error");
	else
		puts("CMYK image ok.");
}


//========================
// RGB 書き込み
//========================


/* Y1行イメージセット (PSD) */

static mlkerr _save_rgb_setrow_ch(mSaveImage *p,int y,int ch,uint8_t *buf,int line_bytes)
{
	uint8_t *ps;
	int i;

	ps = *((uint8_t **)p->param1 + y) + ch;

	for(i = p->width; i; i--, ps += 3)
		*(buf++) = *ps;

	return MLKERR_OK;
}

/** RGB 画像書き込み */

void image_save_rgb(mImageBuf2 *img,const char *filename,int type,int dpi)
{
	mSaveImage si;
	mFuncSaveImage funcs[] = {
		mSaveImagePNG, mSaveImageTIFF, mSaveImagePSD
	};
	int ret;

	mSaveImage_init(&si);

	si.open.type = MSAVEIMAGE_OPEN_FILENAME;
	si.open.filename = filename;
	si.width = img->width;
	si.height = img->height;
	si.coltype = MSAVEIMAGE_COLTYPE_RGB;
	si.bits_per_sample = 8;
	si.samples_per_pixel = 3;
	si.setrow = _save_setrow;
	si.setrow_ch = _save_rgb_setrow_ch;
	si.param1 = img->ppbuf;

	//解像度

	if(dpi)
	{
		si.reso_unit = MSAVEIMAGE_RESOUNIT_DPI;
		si.reso_horz = dpi;
		si.reso_vert = dpi;
	}

	//保存

	ret = (funcs[type])(&si, NULL);

	//

	if(ret)
		puts("! save RGB image error");
	else
		puts("RGB image ok.");
}

