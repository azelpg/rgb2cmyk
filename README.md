# rgb2cmyk

http://azsky2.html.xdomain.jp/

RGB 画像ファイルを、CMYK カラーの画像に変換します。

入力: PNG/TIFF/PSD<br>
出力: TIFF/PSD

## 動作環境

Linux、macOS ほか

## コンパイル/インストール

ninja、pkg-config コマンドが必要です。<br>
各開発用ファイルのパッケージも必要になります。ReadMe を参照してください。

~~~
$ ./configure
$ cd build
$ ninja
# ninja install
~~~
