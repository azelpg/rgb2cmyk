/*$
rgb2cmyk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************
 * RGB -> CMYK 画像変換ツール
 *****************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <lcms2.h>

#include "mlk.h"
#include "mlk_str.h"
#include "mlk_stdio.h"
#include "mlk_charset.h"
#include "mlk_argparse.h"
#include "mlk_imagebuf.h"
#include "mlk_string.h"


//----------------

typedef struct
{
	mStr strInput,		//入力ファイル名
		strOut_cmyk,	//出力ファイル名 (CMYK)
		strOut_rgb,		//出力ファイル名 (RGB)
		strProfile_cmyk,	//ICC プロファイルパス (CMYK)
		strProfile_rgb;		//ICC プロファイルパス (RGB)

	int cmyk_format,	//CMYK 画像出力形式
		rgb_format,		//RGB 画像出力形式
		matching_type,	//マッチング方法
		dpi;			//DPI (出力時)
	uint32_t flags;		//フラグ

	cmsHTRANSFORM trans_cmyk,
		trans_rgb;
}WorkData;

WorkData g_dat;

//----------------

enum
{
	FORMAT_PNG = 0,
	FORMAT_TIFF,
	FORMAT_PSD
};

enum
{
	OPTION_F_AUTO = 1<<0,		//ファイル名自動で出力
	OPTION_F_BLACKPOINT = 1<<1,	//黒点補正
	OPTION_F_EMBED = 1<<2,		//ICC プロファイル埋め込み
	OPTION_F_CMYK_NOCOMP = 1<<3	//CMYK 画像を無圧縮に
};

//---------------

#define VERSION_TEXT "rgb2cmyk ver 1.0.2\nCopyright (c) 2020-2022 Azel"

/* image.c */
mImageBuf2 *image_load(const char *filename,int *dst_dpi,uint8_t **prof_buf,uint32_t *prof_size);
void image_convert_colorspace(mImageBuf2 *img,cmsHTRANSFORM trans);
void image_save_cmyk(mImageBuf2 *img,const char *filename,int type,int dpi,int fnocomp,const char *profile);
void image_save_rgb(mImageBuf2 *img,const char *filename,int type,int dpi);

static void _get_options(int,char **);

//----------------


//========================
// sub
//========================


/** アプリ終了
 *
 * ret: 戻り値 */

static void _exit_app(int ret)
{
	WorkData *p = &g_dat;

	mStrFree(&p->strInput);
	mStrFree(&p->strOut_cmyk);
	mStrFree(&p->strOut_rgb);
	mStrFree(&p->strProfile_cmyk);
	mStrFree(&p->strProfile_rgb);

	exit(ret);
}

/** エラーを表示して、エラー終了させる
 *
 * 終端には改行が出力される。 */

static void puterr_exit(const char *fmt,...)
{
	va_list ap;

	va_start(ap, fmt);
	vprintf(fmt, ap);
	putchar('\n');
	va_end(ap);

	_exit_app(1);
}

//========================


/* プロファイル読み込み */

static cmsHPROFILE _load_profile(const char *filename)
{
	cmsHPROFILE prof;
	FILE *fp;

	fp = mFILEopen(filename, "rb");
	if(!fp) return NULL;

	//[!] fp は自動で閉じられる

	prof = cmsOpenProfileFromStream(fp, "r");
	if(!prof) return NULL;

	return prof;
}

/** lcms 初期化 */

static int _lcms_init(uint8_t *prof_buf,uint32_t prof_size)
{
	cmsHPROFILE prof_rgb,prof_cmyk;
	uint32_t flags;
	char str[256];

	flags = 0;

	//黒点補正

	if(g_dat.flags & OPTION_F_BLACKPOINT)
		flags |= cmsFLAGS_BLACKPOINTCOMPENSATION;

	//RGB プロファイル読み込み

	if(mStrIsnotEmpty(&g_dat.strProfile_rgb))
		//ファイル指定あり
		prof_rgb = _load_profile(g_dat.strProfile_rgb.buf);
	else
	{
		//省略: 入力画像にプロファイルがあればそれを使う。
		//      なければ sRGB

		if(prof_buf)
			prof_rgb = cmsOpenProfileFromMem(prof_buf, prof_size);
		else
			prof_rgb = cmsCreate_sRGBProfile();
	}

	if(!prof_rgb)
	{
		puts("! RGB profile open error");
		return 1;
	}

	if(cmsGetProfileInfoASCII(prof_rgb, cmsInfoDescription, "en", "US", str, 256))
		printf("RGB profile : %s\n", str);

	//CMYK プロファイル読み込み

	prof_cmyk = _load_profile(g_dat.strProfile_cmyk.buf);
	if(!prof_cmyk)
	{
		cmsCloseProfile(prof_rgb);
		puts("! CMYK profile open error");
		return 1;
	}

	if(cmsGetProfileInfoASCII(prof_cmyk, cmsInfoDescription, "en", "US", str, 256))
		printf("CMYK profile: %s\n", str);

	//RGBA -> CMYK

	g_dat.trans_cmyk = cmsCreateTransform(prof_rgb, TYPE_RGBA_8, prof_cmyk, TYPE_CMYK_8,
		g_dat.matching_type, flags);

	//CMYK -> RGB

	g_dat.trans_rgb = cmsCreateTransform(prof_cmyk, TYPE_CMYK_8, prof_rgb, TYPE_RGB_8,
		INTENT_RELATIVE_COLORIMETRIC, 0);

	//プロファイル閉じる

	cmsCloseProfile(prof_rgb);
	cmsCloseProfile(prof_cmyk);

	//エラー

	if(!g_dat.trans_cmyk || !g_dat.trans_rgb)
	{
		if(g_dat.trans_cmyk) cmsDeleteTransform(g_dat.trans_cmyk);
		if(g_dat.trans_rgb) cmsDeleteTransform(g_dat.trans_rgb);
	
		puts("! create transform error");
		return 1;
	}

	return 0;
}

/** 変換処理 */

static void _run_convert(void)
{
	mImageBuf2 *img;
	uint8_t *prof_buf = NULL;
	uint32_t prof_size;
	int ret,srcdpi,dstdpi;

	//ソース読み込み (エラーは関数内で出力される)

	img = image_load(g_dat.strInput.buf, &srcdpi, &prof_buf, &prof_size);
	if(!img) return;

	//lcms 初期化

	ret = _lcms_init(prof_buf, prof_size);

	mFree(prof_buf);

	if(ret)
	{
		mImageBuf2_free(img);
		_exit_app(1);
	}

	//解像度

	dstdpi = (g_dat.dpi)? g_dat.dpi: srcdpi;

	printf("DPI         : (src) %d -> (dst) %d\n", srcdpi, dstdpi);

	//RGB -> CMYK

	image_convert_colorspace(img, g_dat.trans_cmyk);

	if(mStrIsnotEmpty(&g_dat.strOut_cmyk))
	{
		image_save_cmyk(img,
			g_dat.strOut_cmyk.buf,
			(g_dat.cmyk_format == FORMAT_TIFF), dstdpi,
			g_dat.flags & OPTION_F_CMYK_NOCOMP,
			(g_dat.flags & OPTION_F_EMBED)? g_dat.strProfile_cmyk.buf: 0);
	}

	//CMYK -> RGB

	if(mStrIsnotEmpty(&g_dat.strOut_rgb))
	{
		image_convert_colorspace(img, g_dat.trans_rgb);

		image_save_rgb(img,
			g_dat.strOut_rgb.buf, g_dat.rgb_format, dstdpi);
	}

	//解放

	mImageBuf2_free(img);

	cmsDeleteTransform(g_dat.trans_cmyk);
	cmsDeleteTransform(g_dat.trans_rgb);
}

/** main */

int main(int argc,char **argv)
{
	mInitLocale();

	//データ初期化

	mMemset0(&g_dat, sizeof(WorkData));

	g_dat.cmyk_format = FORMAT_PSD;
	g_dat.rgb_format = FORMAT_PNG;
	g_dat.matching_type = INTENT_RELATIVE_COLORIMETRIC;

	//オプション処理

	_get_options(argc, argv);

	//情報

	printf("Input       : ");
	mPutUTF8_stdout(g_dat.strInput.buf);

	printf("\nOutput-CMYK : ");

	if(mStrIsEmpty(&g_dat.strOut_cmyk))
		printf("---");
	else
		mPutUTF8_stdout(g_dat.strOut_cmyk.buf);

	printf("\nOutput-RGB  : ");

	if(mStrIsEmpty(&g_dat.strOut_rgb))
		printf("---");
	else
		mPutUTF8_stdout(g_dat.strOut_rgb.buf);

	putchar('\n');

	//変換処理

	_run_convert();

	//終了

	_exit_app(0);

	return 0;
}


//==============================
// オプション
//==============================


/** キーワードとなる文字列からインデックス取得
 *
 * name : 検索する名前
 * len : 文字数 (-1 で自動)
 * longlist : 長い形式の文字列 (; で区切る)
 * shortlist : 短い形式の文字列。NULL でなし
 * optname : 長い名前のオプション名 (エラー表示時用) */

static int _getopt_keyword(char *name,int len,
	const char *longlist,const char *shortlist,const char *optname)
{
	int ind;

	ind = mStringGetSplitTextIndex(name, len, longlist, ';', FALSE);
	if(ind != -1) return ind;

	if(shortlist)
	{
		ind = mStringGetSplitTextIndex(name, len, shortlist, ';', FALSE);
		if(ind != -1) return ind;
	}

	puterr_exit("--%s: '%s' is invalid", optname, name);

	return -1;
}

/** ファイル名の拡張子からフォーマット取得 */

static int _getformat_from_ext(mStr *strname,const char *optname)
{
	mStr ext = MSTR_INIT;
	int format;

	mStrPathGetExt(&ext, strname->buf);

	if(mStrCompareEq_case(&ext, "png"))
		format = FORMAT_PNG;
	else if(mStrCompareEq_case(&ext, "psd"))
		format = FORMAT_PSD;
	else if(mStrCompareEq_case(&ext, "tiff")
		|| mStrCompareEq_case(&ext, "tif"))
		format = FORMAT_TIFF;
	else
		format = -1;

	mStrFree(&ext);

	if(format == -1)
		puterr_exit("--%s: '.png' or '.psd' or '.tiff' or '.tif'", optname);

	return format;
}


/* --auto */

static void _opt_auto(mArgParse *p,char *arg)
{
	g_dat.flags |= OPTION_F_AUTO;
}

/* --type */

static void _opt_type(mArgParse *p,char *arg)
{
	char *pcdiv;
	int n;

	pcdiv = strchr(arg, ',');
	if(!pcdiv)
		puterr_exit("--type: [TYPE1,TYPE2]");

	//CMYK

	n = _getopt_keyword(arg, pcdiv - arg, "tiff;psd", NULL, "type");

	if(n == 0)
		n = FORMAT_TIFF;
	else
		n = FORMAT_PSD;

	g_dat.cmyk_format = n;

	//RGB

	g_dat.rgb_format = _getopt_keyword(pcdiv + 1, -1, "png;tiff;psd", NULL, "type");
}

/* --cmyk */

static void _opt_cmyk(mArgParse *p,char *arg)
{
	mStrSetText_locale(&g_dat.strOut_cmyk, arg, -1);

	g_dat.cmyk_format = _getformat_from_ext(&g_dat.strOut_cmyk, "cmyk");

	if(g_dat.cmyk_format == FORMAT_PNG)
		puterr_exit("--cmyk: cannot output to png");
}

/* --rgb */

static void _opt_rgb(mArgParse *p,char *arg)
{
	mStrSetText_locale(&g_dat.strOut_rgb, arg, -1);

	g_dat.rgb_format = _getformat_from_ext(&g_dat.strOut_rgb, "rgb");
}

/* --profile-cmyk */

static void _opt_profile_cmyk(mArgParse *p,char *arg)
{
	mStrSetText_locale(&g_dat.strProfile_cmyk, arg, -1);
}

/* --profile-rgb */

static void _opt_profile_rgb(mArgParse *p,char *arg)
{
	mStrSetText_locale(&g_dat.strProfile_rgb, arg, -1);
}

/* --nocomp */

static void _opt_nocomp(mArgParse *p,char *arg)
{
	g_dat.flags |= OPTION_F_CMYK_NOCOMP;
}

/* --dpi */

static void _opt_dpi(mArgParse *p,char *arg)
{
	g_dat.dpi = atoi(arg);

	if(g_dat.dpi < 0) g_dat.dpi = 0;
}

/* --matching */

static void _opt_matching(mArgParse *p,char *arg)
{
	//値は、lcms2 の INTENT_* にそのまま対応している

	g_dat.matching_type = _getopt_keyword(arg, -1,
		"perceptual;relative;saturation;absolute",
		"per;rel;sat;abs", "matching");
}

/* --blackpoint */

static void _opt_blackpoint(mArgParse *p,char *arg)
{
	g_dat.flags |= OPTION_F_BLACKPOINT;
}

/* --embed */

static void _opt_embed(mArgParse *p,char *arg)
{
	g_dat.flags |= OPTION_F_EMBED;
}

/* --version */

static void _opt_version(mArgParse *p,char *arg)
{
	puts(VERSION_TEXT);
	_exit_app(0);
}

/** ヘルプ表示 */

static void _put_help(const char *exename)
{
	printf("Usage: %s [option] INPUTFILE\n\n", exename);

	puts("input image: PNG/TIFF/PSD\n"
"RGB (input) -> CMYK (PSD/TIFF) -> RGB (PNG/PSD/TIFF)\n"
"\nOptions:\n"
"  -a,--auto  output file name is automatic\n"
"         default: './NAME_cmyk.psd' and './NAME_rgb.png'\n"
"  -t,--type=[TYPE1,TYPE2] output format for --auto\n"
"         TYPE1 (CMYK) = psd/tiff\n"
"         TYPE2 (RGB ) = png/psd/tiff\n"
"  -c,--cmyk=[NAME]  output file name of RGB->CMYK image\n"
"         extension is .psd/.tiff/.tif\n"
"         default: do not output.\n"
"         rename file if --auto is present\n"
"  -r,--rgb=[NAME]   output file name of CMYK->RGB image\n"
"         extension is .png/.psd/.tiff/.tif\n"
"         default: do not output\n"
"         rename file if --auto is present\n"
"  -p,--profile-cmyk=[NAME] CMYK ICC profile path\n"
"  -g,--profile-rgb=[NAME]  RGB ICC profile path\n"
"         default: input image profile, sRGB if not\n"
"  -n,--nocomp   uncompress CMYK image\n"
"  -d,--dpi=[N]  DPI value of output image\n"
"         default: get from input file\n"
"  -m,--matching=[TYPE] RGB->CMYK conversion matching method\n"
"         per, perceptual\n"
"         rel, relative [default]\n"
"         sat, saturation\n"
"         abs, absolute\n"
"  -b,--blackpoint black point correction\n"
"  -e,--embed      embed profile in CMYK image\n"
"  -v,--version    display program version\n"
"  -h,--help       display this help"
);

	_exit_app(0);
}

/* --help */

static void _opt_help(mArgParse *p,char *arg)
{
	_put_help(p->argv[0]);
}

/** --auto 時の出力ファイル名をセット */

static void _set_output_auto(void)
{
	const char *ext[] = {"png","tiff","psd"};

	//CMYK
	
	if(mStrIsEmpty(&g_dat.strOut_cmyk))
	{
		mStrPathGetOutputFile_suffix(&g_dat.strOut_cmyk,
			g_dat.strInput.buf, ".", ext[g_dat.cmyk_format], "_cmyk");
	}

	//RGB
	
	if(mStrIsEmpty(&g_dat.strOut_rgb))
	{
		mStrPathGetOutputFile_suffix(&g_dat.strOut_rgb,
			g_dat.strInput.buf, ".", ext[g_dat.rgb_format], "_rgb");
	}
}

/** オプション取得 */

void _get_options(int argc,char **argv)
{
	mArgParse ap;
	mArgParseOpt opts[] = {
	{"auto",'a',0,_opt_auto},
	{"type",'t',1,_opt_type},
	{"cmyk",'c',1,_opt_cmyk},
	{"rgb",'r',1,_opt_rgb},
	{"profile-cmyk",'p',1,_opt_profile_cmyk},
	{"profile-rgb",'g',1,_opt_profile_rgb},
	{"nocomp",'n',0,_opt_nocomp},
	{"dpi",'d',1,_opt_dpi},
	{"matching",'m',1,_opt_matching},
	{"blackpoint",'b',0,_opt_blackpoint},
	{"embed",'e',0,_opt_embed},
	{"version",'v',0,_opt_version},
	{"help",'h',0,_opt_help},
	{0,0,0,0}
	};
	int ind;

	ap.argc = argc;
	ap.argv = argv;
	ap.opts = opts;
	ap.flags = 0;

	ind = mArgParseRun(&ap);

	//エラー

	if(ind < 0)
		_exit_app(1);

	//必須引数が足りない

	if(ind == argc)
		_put_help(argv[0]);

	//入力ファイル名

	mStrSetText_locale(&g_dat.strInput, argv[ind], -1);

	//CMYK プロファイルの指定がない

	if(mStrIsEmpty(&g_dat.strProfile_cmyk))
		puterr_exit("need --profile-cmyk");

	//出力ファイル名の指定がない

	if(!(g_dat.flags & OPTION_F_AUTO)
		&& mStrIsEmpty(&g_dat.strOut_cmyk)
		&& mStrIsEmpty(&g_dat.strOut_rgb))
	{
		puterr_exit("need --auto or --cmyk, --rgb");
	}

	//--auto の場合の出力ファイル名

	if(g_dat.flags & OPTION_F_AUTO)
		_set_output_auto();
}
